import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@/mobile/flexible' // 适配
import '@/styles/reset.css' // 初始化样式
import '@/vant/vant'
import '@vant/touch-emulator'
Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App)
}).$mount('#app')
