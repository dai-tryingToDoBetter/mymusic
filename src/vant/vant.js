import Vue from 'vue'
// 在这里引入你所需的组件
import { Button, Skeleton } from 'vant'
import { Tabbar, TabbarItem } from 'vant'
import { Search } from 'vant'
import { Col, Row } from 'vant'
import { NavBar } from 'vant'
import { Image as VanImage } from 'vant'
import { Cell, CellGroup } from 'vant'
import { Icon } from 'vant'
import { List } from 'vant'
import { Progress } from 'vant'

Vue.use(Progress)
Vue.use(List)
Vue.use(Icon)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(VanImage)
Vue.use(NavBar)
Vue.use(Col)
Vue.use(Row)
Vue.use(Search)
Vue.use(Tabbar)
Vue.use(TabbarItem)
// 按需引入UI组件
Vue.use(Button)
Vue.use(Skeleton)
