import request from '../axios/request'

export const hotSearchAPI = () =>
  request({
    url: '/search/hot'
  })
export const searchResultList = (params) =>
  request({
    url: '/cloudsearch',
    params
  })
