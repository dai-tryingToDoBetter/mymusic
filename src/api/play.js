import request from '../axios/request'
export const getSongPlay = (id) =>
  request({
    url: `/song/detail?ids=${id}`,
    method: 'GET'
  })
export const getLyricById = (id) =>
  request({
    url: `/lyric?id=${id}`,
    method: 'GET'
  })
