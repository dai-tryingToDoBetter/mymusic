import request from '../axios/request'
// 初始化歌单
export const recommendMusic = (params) =>
  request({
    url: '/personalized',
    params
    // 将来外面可能传入params的值 {limit: 20}
  })
// 最新歌单
export const newMusic = (params) =>
  request({
    url: '/personalized/newsong',
    params
  })
