import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/layout'
  },
  {
    path: '/layout',
    name: 'layout',
    redirect: '/layout/home',
    component: () => import('../views/layout/IndexView.vue'),
    children: [
      {
        path: 'home',
        meta: {
          title: '首页'
        },
        component: () => import('../views/HomeView.vue')
      },
      {
        path: 'search',
        component: () => import('../views/AboutView.vue'),
        meta: {
          title: '搜索'
        }
      }
    ]
  },
  {
    path: '/play',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/play/PlayView.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
