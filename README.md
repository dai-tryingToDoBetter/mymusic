# music-me

本项目需要网易云音乐 Node.js API service，必须在服务器启动的情况下才能运行，详情[网易云音乐 Node.js API service](https://github.com/Binaryify/NeteaseCloudMusicApi.git)

## 项目设置

```
npm install
```

### 编译和热重载开发

```
npm run serve
```

### 编译和简化生产

```
npm run build
```

### 检查和修复文件

```
npm run lint
```

### 自定义配置

请参阅 [配置参考](https://cli.vuejs.org/config/).
